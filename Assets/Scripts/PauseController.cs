﻿using UnityEngine;
using System.Collections;

public class PauseController : MonoBehaviour {

    public GameObject PausePanel;

	void Update () {
        if (GameHandler.gameWasOver)
        {
            PausePanel.SetActive(false);
            return;
        }
        if (!PausePanel.activeSelf && Mathf.Approximately(Time.timeScale, 0f))
            PausePanel.SetActive(true);
        else if (PausePanel.activeSelf && Mathf.Approximately(Time.timeScale, 1f))
            PausePanel.SetActive(false);
    }
}
