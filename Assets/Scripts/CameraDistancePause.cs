﻿using UnityEngine;
using System.Collections;

public class CameraDistancePause : MonoBehaviour {

    public Transform tracker;
    public UILabel description;

	void Update() {
        if (!VuforiaController.currentVisibility)
            return;

        if (Vector3.Distance(transform.position, tracker.position) > 37f && Mathf.Approximately(Time.timeScale, 1f)) {
            Time.timeScale = 0f;
            description.text = "The main tracker is too far away.";
        } else if (Vector3.Distance(transform.position, tracker.position) < 37f && Mathf.Approximately(Time.timeScale, 0f))
            Time.timeScale = 1f;
    }

}
