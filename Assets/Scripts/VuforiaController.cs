﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

public class VuforiaController : MonoBehaviour {

	private List<ImageTargetBehaviour> imageTargets;
    public static bool currentVisibility = false;

    public UILabel description;

	void Start () {
		imageTargets = new List<ImageTargetBehaviour>(Resources.FindObjectsOfTypeAll<ImageTargetBehaviour>());
		imageTargets.RemoveAll( itar => itar.TrackableName == "menschena21a" );
        SetVisibility(false);
	}

	void Update () {
		StateManager sm = TrackerManager.Instance.GetStateManager();
		System.Collections.Generic.IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours ();

		bool mainTargetVisible = false;
        foreach (TrackableBehaviour tb in activeTrackables)
            if (tb is ImageTargetBehaviour && tb.TrackableName.Equals("menschena21a"))
                mainTargetVisible = true;

        if (mainTargetVisible && !currentVisibility)
            SetVisibility(true);
        else if (!mainTargetVisible && currentVisibility) {
            SetVisibility(false);
            description.text = "The main target was lost.";
        }
	}

    private void SetVisibility(bool newVis) {
        foreach (ImageTargetBehaviour itar in imageTargets)
            itar.enabled = newVis;

        Time.timeScale = (newVis) ? 1.0f : 0.0f;
        currentVisibility = newVis;
    }
}
